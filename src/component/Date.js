import React, {Component} from 'react';

class Date extends Component{

    render(){
        return(
            <div className="date">
                <div className="date-header">
                    <form onSubmit={this.props.addDate}>
                        <input
                        onChange={this.props.handleDateInput}
                        />
                    </form>
                </div>
            </div>
        )
    }
}

export default Date;