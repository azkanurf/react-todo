import React, {Component} from 'react';
import './App.css';
import TodoList from './TodoList';
import TodoItems from './TodoItems';
import ReactMoment from './ReactMoment';


class App extends Component {
state = {
  items: [],
  currentItem: {text: '', key: ''}
}

handleInput = e => {
  const itemText = e.target.value
  const currentItem = { text: itemText, key: Date.now() }
  this.setState({
    currentItem
  })
  console.log('Hello Input')
}

addItem = e => {
  e.preventDefault()
  const newItem =  this.state.currentItem

  if (newItem.text !== '') {

    console.log (newItem) //
    
    const itemsBaru = [...this.state.items, newItem]
    this.setState({
      items: itemsBaru,
      currentItem: { text: '', key: ''}
    })
  }
  console.log(this.state.currentItem) //
  console.log('Hello Add Item') //
}

deleteItem = key =>{
  const filteredItems = this.state.items.filter(item => {
    return item.key !== key
  })

  // filteredItems itu isinya list2 item yang selain diklik untuk didelete
  this.setState({
    items: filteredItems
  })
  console.log(filteredItems);
  console.log('deleteItem')
}

inputElement = React.createRef()

  render(){
    return (
      <div className="App">
        <TodoList 
        addItem={this.addItem}
        inputElement = {this.inputElement}
        handleInput = {this.handleInput}
        currentItem = {this.state.currentItem}
        />

        <TodoItems 
        entries={this.state.items}
        deleteItem={this.deleteItem}
        />

        {/* <ReactMoment/> */}
      </div>
    );
  }
}

export default App;
